#ifndef _UTILS_H
#define _UTILS_H

#define PAR_CHECK_BYTES      {0x12, 0x34, 0x78, 0x56}

uint32_t opt_to_baudrate(uint16_t baudrate_opt);
uint8_t read_parameters(void * par_buf);
void save_parameters(void * par_buf, uint8_t par_selection);
uint8_t read_config_address(void);
void read_coils(uint8_t *coil_buf);
void set_coils(uint8_t *coil_buf);
void read_discrete_inputs(uint8_t *discrete_input_buf);
void led_ctrl(void);

#endif /* _UTILS_H */
/*
 * FreeModbus Libary: STM8S Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

/* ----------------------- STM8S includes -------------------------------------*/
#include <stm8s_conf.h>
#include <stm8s_it.h>

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Defines ------------------------------------------*/
#define MB_50US_PRESCALER       (800 - 1)

/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Static variables ---------------------------------*/

/* ----------------------- Start implementation -----------------------------*/
BOOL xMBPortTimersInit(USHORT usTim1Timerout50us)
{
    uint16_t period = usTim1Timerout50us - 1;
    
    TIM1_TimeBaseInit(MB_50US_PRESCALER,    // Prescaler value
                      TIM1_COUNTERMODE_UP,  // Counter mode
                      period,               // Period (Autoreload) value
                      0                     // Repetition counter value
                      );

    /* Enable Preload */
    TIM1_ARRPreloadConfig(ENABLE);
    /* Enable TIM1 update Interrupt */
//    TIM1_ITConfig(TIM1_IT_UPDATE, ENABLE);

    vMBPortTimersDisable();

    return TRUE;
}


inline void vMBPortTimersEnable()
{
    /* Clear TIM1_FLAG_UPDATE flag by writing 0. Writing 1 has no effect*/
    TIM1->SR1 = (uint8_t)(~(uint8_t)(TIM1_FLAG_UPDATE));
    /* Enable TIM1 update Interrupt */
    TIM1_ITConfig(TIM1_IT_UPDATE, ENABLE);
    /* Clear counter */
    TIM1->CNTRH = 0x00;
    TIM1->CNTRL = 0x00;
    /* Enable timer */
    TIM1_Cmd(ENABLE);
}

inline void vMBPortTimersDisable()
{
    /* Disable timer */
    TIM1_Cmd(DISABLE);
    /* Clear counter */
    TIM1->CNTRH = 0x00;
    TIM1->CNTRL = 0x00;
    /* Disable TIM1 update Interrupt */
    TIM1_ITConfig(TIM1_IT_UPDATE, DISABLE);
    /* Clear TIM1_FLAG_UPDATE flag by writing 0. Writing 1 has no effect*/
    TIM1->SR1 = (uint8_t)(~(uint8_t)(TIM1_FLAG_UPDATE));
}

void vMBPortTimersDelay(USHORT usTimeOutMS)
{
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired() to notify the protocol stack that
 * the timer has expired.
 */
void vTIMERExpiredISR(void)
{
    (void)pxMBPortCBTimerExpired();
}

#ifndef _BOARD_CONF_H
#define _BOARD_CONF_H

/** LED #0 pin definition. */
#define LED0_NAME           "LED RUN"
#define LED0_PORT           (GPIOD)
#define LED0_PIN            (GPIO_PIN_3)

/** OUTPUT pins definition. */
#define OUT1_PORT           (GPIOC)
#define OUT1_PIN            (GPIO_PIN_3)
#define OUT2_PORT           (GPIOC)
#define OUT2_PIN            (GPIO_PIN_4)
#define OUT3_PORT           (GPIOC)
#define OUT3_PIN            (GPIO_PIN_5)
#define OUT4_PORT           (GPIOC)
#define OUT4_PIN            (GPIO_PIN_6)
#define OUT5_PORT           (GPIOD)
#define OUT5_PIN            (GPIO_PIN_2)
#define OUT6_PORT           (GPIOD)
#define OUT6_PIN            (GPIO_PIN_0)
#define OUT7_PORT           (GPIOC)
#define OUT7_PIN            (GPIO_PIN_7)
#define OUT8_PORT           (GPIOC)
#define OUT8_PIN            (GPIO_PIN_1)

/** INPUT pins definition. */
#define IN1_PORT            (GPIOA)
#define IN1_PIN             (GPIO_PIN_1)
#define IN2_PORT            (GPIOA)
#define IN2_PIN             (GPIO_PIN_2)
#define IN3_PORT            (GPIOA)
#define IN3_PIN             (GPIO_PIN_3)
#define IN4_PORT            (GPIOF)
#define IN4_PIN             (GPIO_PIN_4)
#define IN5_PORT            (GPIOB)
#define IN5_PIN             (GPIO_PIN_7)
#define IN6_PORT            (GPIOB)
#define IN6_PIN             (GPIO_PIN_6)
#define IN7_PORT            (GPIOB)
#define IN7_PIN             (GPIO_PIN_3)
#define IN8_PORT            (GPIOB)
#define IN8_PIN             (GPIO_PIN_2)

/** UART pins definition. */
#define UART1_TX_PORT       (GPIOD)
#define UART1_TX_PIN        (GPIO_PIN_5)
#define UART1_RX_PORT       (GPIOD)
#define UART1_RX_PIN        (GPIO_PIN_6)
/** RS485 DE/nRE pins definition. */
//#define RS485_DR_PORT       (GPIOD)
//#define RS485_DR_PIN        (GPIO_PIN_6)

/**
 * \brief Turns off the specified LEDs.
 *
 * \param led LED to turn off (LEDx_PIN).
 *
 * \note The pins of the specified LEDs are set to GPIO output mode.
 */
#define LED_Off(led)     GPIO_WriteLow(led##_PORT, led##_PIN)

/**
 * \brief Turns on the specified LEDs.
 *
 * \param led LED to turn on (LEDx_PIN).
 *
 * \note The pins of the specified LEDs are set to GPIO output mode.
 */
#define LED_On(led)      GPIO_WriteHigh(led##_PORT, led##_PIN)

/**
 * \brief Toggles the specified LEDs.
 *
 * \param led LED to toggle (LEDx_PIN).
 *
 * \note The pins of the specified LEDs are set to GPIO output mode.
 */
#define LED_Toggle(led)  GPIO_WriteReverse(led##_PORT, led##_PIN)

enum par_type {
    PAR_ALL,
    PAR_BAUDRATE_OPT,
    PAR_RSV,
    PAR_OFFSET_ADDR,
    PAR_OP_MODE,
    PAR_DELAY_TIME
};

#define PAR_BAUDRATE_OPT_OFFSET     0
#define PAR_OFFSET_ADDR_OFFSET      4
#define PAR_OP_MODE_OFFSET          6
#define PAR_DELAY_TIME_OFFSET       8

typedef enum {
    BR_DFT = (uint16_t)0,
    BR_2400 = (uint16_t)1,
    BR_4800 = (uint16_t)2,
    BR_9600 = (uint16_t)3,
    BR_19200 = (uint16_t)4,
    BR_38400 = (uint16_t)5
} BaudrateOpt_TypeDef;

typedef enum {
    OM_SEQUENTIAL_ON = (uint16_t)1,
    OM_LOOP_RUN = (uint16_t)2,
    OM_MARQUEE = (uint16_t)3,
    OM_MANUAL = (uint16_t)4,
    OM_ON_OFF = (uint16_t)5,
    OM_OFF_ON = (uint16_t)6
} OperationMode_TypeDef;

typedef struct
{
  uint16_t baudrate_opt;    /*!< RS232 or RS485 baudrate option.*/
  uint16_t rsv;             /*!< Reserved.*/
  uint16_t offset_addr;     /*!< Device offset address.*/
  uint16_t op_mode;         /*!< Operation mode.*/
  uint16_t delay_time;      /*!< Delay time in 100ms.*/
} device_parameters_t;

#define DEFAULT_BAUDRATE_OPT    BR_9600
#define DEFAULT_OFFSET_ADDR     1
#define DEFAULT_OP_MODE         OM_MANUAL
#define DEFAULT_DELAY_TIME      10


#endif /* _BOARD_CONF_H */